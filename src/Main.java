import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by trae on 1/11/2016.
 */
public class Main {

    static int  PNum[] = new int[15];
    static String PLname[] = new String[15];
    static String DName[] = new String[15];
    static String Operation[] = new String[15];
    static double cost[] = new double[15];

    public static void ReadFile(File data) throws FileNotFoundException {
            Scanner eyes = new Scanner(data);
        for(int x = 0; x < 15; x++) {
            PNum[x]= Integer.valueOf(eyes.next());
            PLname[x] = eyes.next();
            DName[x] = eyes.next();
            Operation[x] = eyes.next();
            cost[x] = Double.valueOf(eyes.next());
        }

    }

    public static void Report(){
        Scanner eyes = new Scanner(System.in);
        int c = 0;
        while(c != 6){
        System.out.println("1)List of all information");
        System.out.println("2)List of all surgeries for a specific doctor (prompt for the doctor)");
        System.out.println("3)List of all surgeries of a specific type (prompt for the surgery type)");
        System.out.println("4)Total amount of surgery fees paid to each Doctor");
        System.out.println("5)Average operation price");
        System.out.println("6)Exit");
        System.out.print(">");
        c = eyes.nextInt();
        switch(c) {
            case 1:
                for(int x = 0; x < 15;x++){
                   System.out.print(PNum[x]);
                    System.out.print(" ");
                    System.out.print(PLname[x]);
                    System.out.print(" ");
                    System.out.print(DName[x]);
                    System.out.print(" ");
                    System.out.print(Operation[x]);
                    System.out.print(" ");
                    System.out.println(cost[x]);
                    System.out.println("------");
                }
                break;
            case 2:
                ListDoctors(DName,Operation);
                break;
            case 3:
                ListSurgeries(Operation,PNum);
                break;
            case 4:
                HashMap<String,Double> fees = new HashMap();
                for(int x = 0; x < 15;x++){
                    if(fees.containsKey(DName[x])){
                        double current = fees.get(DName[x])+cost[x];
                        fees.remove(DName[x]);
                        fees.put(DName[x],current);
                    }
                    else
                        fees.put(DName[x],cost[x]);
                }
                System.out.println(fees);
                break;
            case 5:
                double total=0;
                for(double price :cost)
                    total += price;
                System.out.print("The Average Opeation Price was $" );
                System.out.println(total);

                break;
            case 6:
                return;
        }
        }
    }

    private static void ListSurgeries(String operation[],int Pnum[]){
        Scanner eyes = new Scanner(System.in);
        System.out.print("Enter name of Operation of interest:");
        String Op = eyes.next();
        System.out.println(Op + " has the following patients:");
        for(int x = 0 ;x < operation.length;x++)
            if(Op.equalsIgnoreCase(operation[x]))
                System.out.print(Pnum[x]+"\n");
    }
    private static void ListDoctors(String Dname[],String Operations[]) {
        Scanner eyes = new Scanner(System.in);
        System.out.print("Enter name of doctor of interest:");
        String dName = eyes.next();
        System.out.println(dName+" has the following Operations:");
        for(int x = 0 ;x < Dname.length;x++){
            if(dName.equalsIgnoreCase(Dname[x]))
                System.out.print(Operations[x]+"\n");

        }

    }
    public static void main(String[] args) throws FileNotFoundException {
        ReadFile(new File("C:\\Users\\trae\\GoogleDrive\\Programming Workspace\\Adv. Java\\src\\Data.txt"));
        Report();


    }
}
